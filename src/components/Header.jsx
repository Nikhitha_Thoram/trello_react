import React from "react";
import { Link } from "react-router-dom";
import homeIcon from "../assets/home.png";
import userIcon from "../assets/user.png";

function Header() {
  return (
    <div>
      <header className="flex flex-row justify-between items-center bg-gray-300  h-16 p-4">
        <Link to={"/"}>
          <img src={homeIcon} className="w-8 h-8 bg-none" alt="logo" />
        </Link>
        <img
          className="w-[120px] h-[50px] block icon py-2"
          src="https://trello.com/assets/d947df93bc055849898e.gif"
          alt="logo"
        ></img>
        <img src={userIcon} className="w-8 h-8 bg-none" alt="logo" />
      </header>
    </div>
  );
}

export default Header;
