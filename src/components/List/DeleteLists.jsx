import React from "react";
import { apiKey, apiToken } from "../Api";
import axios from "axios";
import { IconButton } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";

function DeleteLists({ listId, onDelete }) {
  const handleDelete = () => {
    const url = `https://api.trello.com/1/lists/${listId}/closed?key=${apiKey}&token=${apiToken}&value=true`;
    axios
      .put(url)
      .then(() => {
        onDelete(listId);
      })
      .catch((error) => {
        console.error(`Error deleting list ${listId}:`, error);
      });
  };
  return (
    <div>
      <IconButton
        aria-label="Delete List"
        icon={<CloseIcon />}
        variant="ghost"
        colorScheme="black"
        sx={{ fontSize: '13px' }}
        onClick={handleDelete}
      />
    </div>
  );
}

export default DeleteLists;
