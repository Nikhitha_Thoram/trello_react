import React from "react";
import { Button } from "@chakra-ui/react";
import ModalPopup from "../ModalPopup";
import { useDisclosure } from "@chakra-ui/react";
import { apiKey, apiToken } from "../Api";
import axios from "axios";

function CreateList({ updateLists, boardId }) {
  const createNewList = (listName) => {
    const url = `https://api.trello.com/1/lists/?name=${listName}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`;
    axios
      .post(url)
      .then((response) => {
        updateLists(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Button width={"64"} height={"24"} onClick={onOpen}>
        Create New List
      </Button>
      <ModalPopup
        isOpen={isOpen}
        onOpen={onOpen}
        onClose={onClose}
        title="Create New List"
        onSubmit={createNewList}
        inputLabel="List Title"
        inputPlaceholder="Enter List Name..."
      />
    </>
  );
}

export default CreateList;
