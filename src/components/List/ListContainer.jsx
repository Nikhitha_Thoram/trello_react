import React from "react";
import { useState, useEffect } from "react";
import { apiKey, apiToken } from "../Api";
import axios from "axios";
import { Box, Flex, Text } from "@chakra-ui/react";
import CreateList from "./CreateList";
import DeleteLists from "./DeleteLists";
import CardContainer from "../cards/CardContainer";

function ListContainer({ boardId }) {
  const [lists, setLists] = useState([]);
  const updateLists = (newListData) => {
    setLists([...lists, newListData]);
  };

  useEffect(() => {
    const url = `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${apiToken}`;
    axios
      .get(url)
      .then((response) => {
        setLists(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data", error);
      });
  }, [boardId]);

  const deleteLists = (listId) => {
    const deletedLists = lists.filter((list) => list.id !== listId);
    setLists(deletedLists);
  };

  return (
    <Flex wrap="wrap" gap={4} p={4}>
      {lists.map((listData) => (
        <Box
          key={listData.id}
          bg="gray.200"
          p={4}
          borderRadius="md"
          boxShadow="md"
          maxW="300px"
          w="100%"
        >
          <Flex mb={4} alignItems="center" justifyContent="space-between">
            <Text fontSize="lg" fontWeight="bold" color="gray.700">
              {listData.name}
            </Text>
            <DeleteLists listId={listData.id} onDelete={deleteLists} />
          </Flex>
          <Box>
            <CardContainer listId={listData.id} />
          </Box>
        </Box>
      ))}

      <CreateList updateLists={updateLists} boardId={boardId} />
    </Flex>
  );
}

export default ListContainer;
