import React from "react";
import { useParams } from "react-router-dom";
import ListContainer from "../List/ListContainer";

function BoardDetails() {
  const { id } = useParams();
  const { name } = useParams();

  return (
    <div className="listData mt-10 ml-10">
      <h2 className="text-xl text-black ml-4">{name}</h2>
      <ListContainer boardId={id} />
    </div>
  );
}

export default BoardDetails;
