import React from "react";
import { Divider } from "@chakra-ui/react";

function UserDetails() {
  return (
    <div className="userDeatils">
      <div className="text_container flex flex-row gap-3 items-center m-6 ml-20">
        <p className="text-white bg-gradient-to-r from-[#2d9b6d] to-[#38AE7D] w-[55px] h-[55px] text-[30px] p-1 text-bold text-center rounded-sm">
          N
        </p>
        <div class="left_container">
          <h2>Nikhitha's Workspace</h2>
          <div className="details">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              class="size-4 float-left"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M16.5 10.5V6.75a4.5 4.5 0 1 0-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 0 0 2.25-2.25v-6.75a2.25 2.25 0 0 0-2.25-2.25H6.75a2.25 2.25 0 0 0-2.25 2.25v6.75a2.25 2.25 0 0 0 2.25 2.25Z"
              />
            </svg>
            <p className="text-sm">Private</p>
          </div>
        </div>
      </div>
      <Divider my={8} borderColor={"#d7dbd9"} width={"95%"} ml={8} />
    </div>
  );
}

export default UserDetails;
