import React from "react";
import { Button } from "@chakra-ui/react";
import { useDisclosure } from "@chakra-ui/react";
import { apiKey, apiToken } from "../Api";
import axios from "axios";
import ModalPopup from "../ModalPopup";

function CreateBoard({ updateBoards }) {
  const createNewBoard = (boardName) => {
    const url = `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`;
    axios
      .post(url)
      .then((response) => {
        updateBoards(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Button width={"64"} height={"24"} onClick={onOpen}>
        Create New Board
      </Button>
      <ModalPopup
        isOpen={isOpen}
        onOpen={onOpen}
        onClose={onClose}
        title="Create New Board"
        onSubmit={createNewBoard}
        inputLabel="Board Title"
        inputPlaceholder="Enter Board Name..."
      />
    </>
  );
} 

export default CreateBoard;
