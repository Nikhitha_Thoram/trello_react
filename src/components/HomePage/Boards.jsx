import React from "react";
import { apiKey, apiToken } from "../Api";
import { useState, useEffect } from "react";
import axios from "axios";
import { Spinner } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import CreateBoard from "./CreateBoard";
import UserDetails from "./UserDetails";

function Boards() {
  const [boards, setBoards] = useState([]);
  const [loader, setLoader] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${apiToken}`
      )
      .then((response) => {
        setBoards(response.data);
        setLoader(false);
      })

      .catch((error) => {
        console.error("Error fetching data", error);
        setError(error);
      });
  }, []);

  const updateBoards = (newBoardData) => {
    setBoards([...boards, newBoardData]);
  };

  return (
    <div>
      <UserDetails />
      <h1 className="text-black font-bold ml-20 text-2xl">Boards</h1>

      <div className="main_Conatiner">
        {error ? (
          <div className="error_message text-center text-2xl text-red-500">
            Something went wrong.Please try again Later!
          </div>
        ) : (
          <>
            {loader ? (
              <div className="spinner text-center">
                <Spinner
                  thickness="4px"
                  speed="0.65s"
                  emptyColor="gray.200"
                  color="blue.500"
                  size="xl"
                />
              </div>
            ) : (
              <div className="boards_container flex flex-row flex-wrap gap-6 ml-20 mt-6 w-[1600px]">
                {boards.map((boardData) => (
                  <Link to={`/boards/${boardData.id}/${boardData.name}`}>
                    <div
                      key={boardData.id}
                      className="board w-64 h-24 text-white rounded-md font-bold flex items-start p-2 text-xl"
                      style={{
                        backgroundColor: boardData.prefs.backgroundColor
                          ? boardData.prefs.backgroundColor
                          : "#287fbd",
                        backgroundImage: boardData.prefs.backgroundImage
                          ? `url(${boardData.prefs.backgroundImage})`
                          : "none",
                        backgroundSize: "cover",
                        backgroundPosition: "center",
                      }}
                    >
                      <h2>{boardData.name}</h2>
                    </div>
                  </Link>
                ))}
                <CreateBoard updateBoards={updateBoards} />
              </div>
            )}
          </>
        )}
      </div>
    </div>
  );
}

export default Boards;
