import React from "react";
import axios from "axios";
import { apiKey, apiToken } from "../Api";
import { useState, useEffect } from "react";
import { CheckCircleIcon } from "@chakra-ui/icons";
import CreateCheckLists from "./CreateCheckLists";
import DeleteCheckLists from "./DeleteCheckLists";
import CheckItemsContainer from "../CheckItems/CheckItemsContainer";

import { Box, Text, Flex } from "@chakra-ui/react";

function CheckListContainer({ cardId }) {
  const [checkLists, setCheckLists] = useState([]);

  const updateCheckLists = (newCheckList) => {
    setCheckLists([...checkLists, newCheckList]);
  };

  useEffect(() => {
    const url = `https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${apiToken}`;
    axios
      .get(url)
      .then((response) => {
        setCheckLists(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data", error);
      });
  }, []);

  const deleteCheckLists = (checkListId) => {
    const deletedItems = checkLists.filter(
      (checkList) => checkList.id !== checkListId
    );
    setCheckLists(deletedItems);
  };

  return (
    <div>
      <div className="flex justify-end mb-6">
        <CreateCheckLists updateCheckLists={updateCheckLists} cardId={cardId} />
      </div>
      <div>
        {checkLists.map((item, index) => (
          <div key={index}>
            <Flex mb={2} alignItems="center" justifyContent="space-between">
              <Flex alignItems="center" gap={2}>
                <CheckCircleIcon />
                <h1 className="font-bold">{item.name}</h1>
              </Flex>
              <DeleteCheckLists
                checkListId={item.id}
                onDelete={deleteCheckLists}
              />
            </Flex>
            <Box pl={4} mb={4}>
              <CheckItemsContainer cardId={cardId} checkListId={item.id} />
            </Box>
          </div>
        ))}
      </div>
    </div>
  );
}

export default CheckListContainer;
