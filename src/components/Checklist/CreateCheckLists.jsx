import React from "react";
import { Button } from "@chakra-ui/react";
import ModalPopup from "../ModalPopup";
import { useDisclosure } from "@chakra-ui/react";
import { apiKey, apiToken } from "../Api";
import axios from "axios";

function CreateCheckLists({updateCheckLists, cardId }) {
  const createNewCheckList = (checkListName) => {
    const url = `https://api.trello.com/1/cards/${cardId}/checklists?name=${checkListName}&key=${apiKey}&token=${apiToken}`;
    axios
      .post(url)
      .then((response) => {
        updateCheckLists(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data", error);
      });
  };

  const { isOpen, onOpen, onClose } = useDisclosure();
  
  return (
    <>
      <Button onClick={onOpen} width={"24"} height={"10"} fontSize="sm" color="gray.600">
        + checklist
      </Button>
      <ModalPopup
        isOpen={isOpen}
        onOpen={onOpen}
        onClose={onClose}
        title="Create New CheckList"
        onSubmit={createNewCheckList}
        inputLabel="CheckList Title"
        inputPlaceholder="Enter CheckList Name..."
      />
    </>
  );
}

export default CreateCheckLists;
