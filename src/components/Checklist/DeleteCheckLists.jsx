import React from "react";
import { apiKey, apiToken } from "../Api";
import axios from "axios";
import { Button } from "@chakra-ui/react";


function DeleteCheckLists({checkListId,onDelete}) {
    const handleDelete=()=>{
        const url = `https://api.trello.com/1/checklists/${checkListId}?key=${apiKey}&token=${apiToken}`;
        axios.delete(url)
        .then(()=>{
            onDelete(checkListId);
        })
        .catch((error)=>{
            console.error(error);
        })
    }
    
  return (
    <div>
       <Button onClick={handleDelete} width={"24"} height={"10"} fontSize="sm" color="gray.600">
        Delete
      </Button>
    </div>
  )
}

export default DeleteCheckLists;