import React from "react";
import { apiKey, apiToken } from "../Api";
import axios from "axios";
import { IconButton } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";


function DeleteCards({cardId,onDelete}) {
    const handleDelete=(event)=>{
      event.stopPropagation();
        const url=`https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${apiToken}`
        axios.delete(url)
        .then(()=>{
            onDelete(cardId);
        })
        .catch((error)=>{
            console.error(error);
        })
    }

  return (
    <div>
      <IconButton
        aria-label="Delete Card"
        icon={<CloseIcon/>}
        variant="ghost"
        colorScheme="black"
        sx={{ fontSize: '10px' }}
        onClick={handleDelete}
      />
    </div>
  )
}

export default DeleteCards;