import React from "react";
import { Button } from "@chakra-ui/react";
import ModalPopup from "../ModalPopup";
import { useDisclosure } from "@chakra-ui/react";
import { apiKey, apiToken } from "../Api";
import axios from "axios";

function CreateCards({ updateCards, listId }) {
  const createNewCard = (cardName) => {
    const url = `https://api.trello.com/1/cards/?name=${cardName}&idList=${listId}&key=${apiKey}&token=${apiToken}`;
    axios
      .post(url)
      .then((response) => {
        updateCards(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data", error);
      });
  };

  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Button onClick={onOpen} bg="transparent" fontSize="sm" color="gray.600">
        + Add a Card
      </Button>
      <ModalPopup
        isOpen={isOpen}
        onOpen={onOpen}
        onClose={onClose}
        title="Create New Card"
        onSubmit={createNewCard}
        inputLabel="Card Title"
        inputPlaceholder="Enter Card Name..."
      />
    </>
  );
}

export default CreateCards;
