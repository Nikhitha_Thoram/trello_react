import React from "react";
import axios from "axios";
import { apiKey, apiToken } from "../Api";
import { useState, useEffect } from "react";
import { useDisclosure } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";

import {
  Box,
  Text,
  Flex,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  IconButton,
} from "@chakra-ui/react";

import CreateCards from "./CreateCards";
import DeleteCards from "./DeleteCards";
import CheckListContainer from "../Checklist/CheckListContainer";

function CardContainer({ listId }) {
  const [cards, setCards] = useState([]);
  const [selectedCard, setSelectedCard] = useState(null);

  const { isOpen, onOpen, onClose } = useDisclosure();

  const updateCards = (newCard) => {
    setCards([...cards, newCard]);
  };

  useEffect(() => {
    const url = `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${apiToken}`;
    axios
      .get(url)
      .then((response) => {
        setCards(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data", error);
      });
  },[]);

  const deleteCards = (cardId) => {
    const deletedCards = cards.filter((card) => card.id !== cardId);
    setCards(deletedCards);
  };

  const handleCardClick = (card) => {
    setSelectedCard(card);
    onOpen();
  };

  return (
    <div>
      <Box>
        {cards.map((card) => (
          <Box
            key={card.id}
            bg="white"
            p={2}
            borderRadius="md"
            mb={2}
            boxShadow="sm"
            cursor="pointer"
            _hover={{ bg: "gray.100" }}
            onClick={() => handleCardClick(card)}
          >
            <Flex mb={4} alignItems="center" justifyContent="space-between">
              <Text>{card.name}</Text>
              <DeleteCards cardId={card.id} onDelete={deleteCards} />
            </Flex>
          </Box>
        ))}
        <CreateCards updateCards={updateCards} listId={listId} />
      </Box>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader
            display="flex"
            justifyContent="space-between"
            alignItems="center"
          >
            Checklist for {selectedCard?.name}
            <IconButton
              aria-label="Delete List"
              icon={<CloseIcon />}
              variant="ghost"
              colorScheme="black"
              sx={{ fontSize: "13px" }}
              onClick={onClose}
            />
          </ModalHeader>
          <ModalBody>
            <CheckListContainer cardId={selectedCard?.id} />
          </ModalBody>
        </ModalContent>
      </Modal>
    </div>
  );
}

export default CardContainer;
