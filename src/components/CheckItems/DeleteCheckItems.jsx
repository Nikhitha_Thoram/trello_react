import React from "react";
import { apiKey, apiToken } from "../Api";
import axios from "axios";
import { IconButton } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";

function DeleteCheckItems({checkListId,checkItemId,onDelete}) {
    const handleDelete=()=>{
        const url = `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${apiKey}&token=${apiToken}`
        axios.delete(url)
        .then(()=>{
            onDelete(checkItemId);
        })
        .catch((error)=>{
            console.error(error);
        })
    }

  return (
    <div>
       <IconButton
        aria-label="Delete Card"
        icon={<CloseIcon/>}
        variant="ghost"
        colorScheme="black"
        sx={{ fontSize: '8px' }}
        onClick={handleDelete}
      />
    </div>
  )
}

export default DeleteCheckItems;