import React from "react";
import { Button } from "@chakra-ui/react";
import ModalPopup from "../ModalPopup";
import { useDisclosure } from "@chakra-ui/react";
import { apiKey, apiToken } from "../Api";
import axios from "axios";

function CreateCheckItems({ updateCheckItems, checkListId }) {
  const createNewCheckItem = (checkItemName) => {
    const url = `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkItemName}&key=${apiKey}&token=${apiToken}`;
    axios
      .post(url)
      .then((response) => {
        updateCheckItems(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data", error);
      });
  };

  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Button
        onClick={onOpen}
        width={"25"}
        height={"10"}
        fontSize="sm"
        color="gray.600"
        ml={8}
      >
        Add an item
      </Button>
      <ModalPopup
        isOpen={isOpen}
        onOpen={onOpen}
        onClose={onClose}
        title="Create New CheckItem"
        onSubmit={createNewCheckItem}
        inputLabel="CheckItem Title"
        inputPlaceholder="Enter CheckItem Name..."
      />
    </>
  );
}

export default CreateCheckItems;
