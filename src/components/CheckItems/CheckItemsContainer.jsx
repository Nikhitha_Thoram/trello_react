import React from "react";
import axios from "axios";
import { apiKey, apiToken } from "../Api";
import { useState, useEffect } from "react";
import { Checkbox } from "@chakra-ui/react";
import { Flex } from "@chakra-ui/react";
import { Progress } from "@chakra-ui/react";
import CreateCheckItems from "./CreateCheckItems";
import DeleteCheckItems from "./DeleteCheckItems";

function CheckItemsContainer({ checkListId, cardId }) {
  const [checkItems, setCheckItems] = useState([]);
  const [progress, setProgress] = useState(0);

  const updateCheckItems = (newCheckItem) => {
    const updateItems = [...checkItems, newCheckItem];
    setCheckItems(updateItems);
    calculateProgress(updateItems);
  };

  useEffect(() => {
    const url = `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${apiKey}&token=${apiToken}`;
    axios
      .get(url)
      .then((response) => {
        setCheckItems(response.data);
        calculateProgress(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data", error);
      });
  },[]);

  const deleteCheckItems = (checkItemId) => {
    const deletedItems = checkItems.filter(
      (checkItem) => checkItem.id !== checkItemId
    );
    setCheckItems(deletedItems);
    calculateProgress(deletedItems);
  };

  const toggleCheckItem = (checkItemId) => {
    const currentItem = checkItems.find((item) => item.id === checkItemId);
    const newState =
      currentItem.state === "complete" ? "incomplete" : "complete";
    const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${apiKey}&token=${apiToken}&state=${newState}`;
    axios
      .put(url)
      .then(() => {
        const updatedItems = checkItems.map((item) =>
          item.id === checkItemId ? { ...item, state: newState } : item
        );
        setCheckItems(updatedItems);
        calculateProgress(updatedItems);
      })
      .catch((error) => {
        console.error("Error updating item state", error);
      });
  };

  const calculateProgress = (items) => {
    if (items.length === 0) {
      setProgress(0);
    } else {
      const completedItems = items.filter((item) => item.state === "complete");
      const progressValue = (completedItems.length / items.length) * 100;
      setProgress(progressValue);
    }
  };
  
  return (
    <div>
      <div>
        <Progress
          value={progress}
          colorScheme={progress === 100 ? "green" : "blue"}
          size="sm"
          marginTop={2}
          mb={4}
        />
      </div>
      <div>
        {checkItems.map((item, index) => (
          <div>
            <Flex alignItems="center" justifyContent="space-between">
              <ul>
                <Flex gap={1}>
                  <Checkbox
                    borderColor="gray.400"
                    isChecked={item.state === "complete"}
                    onChange={() => toggleCheckItem(item.id)}
                  ></Checkbox>
                  <li className="m-2" key={index}>
                    {item.name}
                  </li>
                </Flex>
              </ul>
              <DeleteCheckItems
                checkListId={checkListId}
                checkItemId={item.id}
                onDelete={deleteCheckItems}
              />
            </Flex>
          </div>
        ))}
      </div>
      <div>
        <CreateCheckItems
          checkListId={checkListId}
          updateCheckItems={updateCheckItems}
        />
      </div>
    </div>
  );
}

export default CheckItemsContainer;
