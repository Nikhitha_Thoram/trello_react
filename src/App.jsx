import { useState } from 'react'
import './App.css';
import Header from './components/Header';
import Boards from './components/HomePage/Boards';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import BoardDetails from './components/Boards/BoardDetails';

function App() {

  return (
    <div>
       <Router>
      <div>
        <Header />
        <Routes>
        <Route path="/" element={<Boards />} />
        <Route path="/boards/:id/:name" element={<BoardDetails/>}/>
        </Routes>
      </div>
    </Router>
    </div>
  )
}

export default App
